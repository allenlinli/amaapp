//
//  APIManagerTests.swift
//  AmaApp
//
//  Created by Li Lin2 on 12/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import XCTest
@testable import AmaApp

class APIManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetJsonDictionary() {
        let exp = expectation(description: "parse json from api call (file)")
        
        APIManager.sharedInstance.getJsonDictionary { (dictionary, error) in
            XCTAssert(error == nil)
            XCTAssert(dictionary["data"] != nil)
            XCTAssert(dictionary["included"] != nil)
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssert(error == nil)
        }
    }
    
    func testGetValues() {
        let exp = expectation(description: "parse json from api call (file)")
        
        APIManager.sharedInstance.getJsonValues { (tuple, error) in
            XCTAssert(tuple != nil)
            
            let account = tuple?.0
            let service = tuple?.1
//            let subscription = tuple?.2
            let products = tuple?.3
            
            XCTAssert(account != nil)
            XCTAssert(service?.msn == "0468874507")
            XCTAssert(service?.credit == 1200)
            
            XCTAssert(products != nil)
            XCTAssert((products?.count)! > 0)
            let product = products?.last
            XCTAssert(product?.name == "UNLIMITED 7GB")
            XCTAssert(product?.price == 3990)
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssert(error == nil)
        }
    }
    
}
