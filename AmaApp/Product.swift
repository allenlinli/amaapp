//
//  Product.swift
//  AmaApp
//
//  Created by Li Lin2 on 12/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import Foundation

struct Product {
    
//    var resourceObjectID : String
//    var resourceObjectType : String
    
    var name : String
//    var includedData : Float?
//    var includedCredit : Int?
//    var includedInternationalTalk : Float? = -1.0
//    var unlimitedText : Bool = false
//    var unlimitedTalk : Bool = false
//    var unlimitedInternationalText : Bool = false
//    var unlimitedInternationalTalk : Bool = false
    var price : Int
    
    //    {
    //    "type":"products",
    //    "id":"4000",
    //    "attributes":{
    //    "name":"UNLIMITED 7GB",
    //    "included-data":null,
    //    "included-credit":null,
    //    "included-international-talk":null,
    //    "unlimited-text":true,
    //    "unlimited-talk":true,
    //    "unlimited-international-text":false,
    //    "unlimited-international-talk":false,
    //    "price":3990
    //    }
    //    }
}
