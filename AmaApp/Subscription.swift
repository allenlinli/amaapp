//
//  Subscription.swift
//  AmaApp
//
//  Created by Li Lin2 on 12/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import Foundation

struct Subscription {
    
//    var resourceObjectID : String
//    var resourceObjectType : String
//    
    var includedDataBalance : Float
//    var includedCreditBalance : Int?
//    var includedRolloverDataBalance : Float?
//    var includedRolloverCreditBalance : Int?
//    var includedInternationalTalkBalance : Float?
//    var expiryDate : Date?
//    var autoRenewal : Bool
//    var primarySubscription : Bool
//    var selfLink : String
//    
//    var relatedServiceLink : String
//    
//    var relatedProductLinkID : String
//
//    var relatedDownGradeLink : String?
//    var relatedDownGradeID : String?
    
    //    {
    //    "type":"subscriptions",
    //    "id":"0468874507-0",
    //    "attributes":{
    //    "included-data-balance":52400,
    //    "included-credit-balance":null,
    //    "included-rollover-credit-balance":null,
    //    "included-rollover-data-balance":null,
    //    "included-international-talk-balance":null,
    //    "expiry-date":"2016-11-19",
    //    "auto-renewal":true,
    //    "primary-subscription":true
    //    },
    //    "links":{
    //    "self":"http://localhost:3000/services/0468874507/subscriptions/0468874507-0"
    //    },
    //    "relationships":{
    //    "service":{
    //    "links":{
    //    "related":"http://localhost:3000/services/0468874507"
    //    }
    //    },
    //    "product":{
    //    "data":{
    //    "type":"products",
    //    "id":"0"
    //    }
    //    },
    //    "downgrade":{
    //    "data":null
    //    }
    //    }
    //    },
}
