//
//  ProductViewController.swift
//  AmaApp
//
//  Created by Li Lin2 on 12/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {

    @IBOutlet weak var productNameLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    deinit {
        // NotificationCenter.default.removeObserver(self)
        // we don't need to remove observer: http://stackoverflow.com/a/40339926/1640653
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateView), name: Notification.Name(rawValue:Constants.NotoficationKeys.sharedViewModelChanged), object: nil)
        
        updateView()
    }
    
    func updateView() {
        if let name = SharedViewModel.sharedInstance.products?.last?.name {
            productNameLabel.text = name
        }
        
        if let price = SharedViewModel.sharedInstance.products?.last?.price {
            let priceInDollars = Double(price) / 100.0
            priceLabel.text = String(format: "$%.2f", priceInDollars)
        }
    }
}

