//
//  SharedViewModel.swift
//  AmaApp
//
//  Created by Li Lin2 on 13/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import Foundation

class SharedViewModel : NSObject {
    
    static let sharedInstance = SharedViewModel()
    
    private(set) var service : Service?
    private(set) var account : Account?
    private(set) var subscription : Subscription?
    private(set) var products : [Product]?
    
    private override init() {
        
        super.init()
        
        getValues { (tuple, error) in
        }
    }
    
    func getValues(_ completion:@escaping ((Account, Service, Subscription, [Product])?, _ error: Error?)->Void ) {
        APIManager.sharedInstance.getJsonValues { [unowned self] (tuple, error) in
            if let error = error {
                completion(nil,error)
                return
            }
            
            guard let tuple = tuple else {
                assert(false)
            }
            self.account = tuple.0
            self.service = tuple.1
            self.subscription = tuple.2
            self.products = tuple.3
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NotoficationKeys.sharedViewModelChanged), object: nil)
            completion(tuple, nil)
        }
    }
}
