//
//  WelcomeViewController.swift
//  AmaApp
//
//  Created by Li Lin2 on 12/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        UserDefaults.standard.set(true, forKey: Constants.UserDefaultsKeys.hasShownWelcomePage)
        // Do any additional setup after loading the view.
    }

    @IBAction func welcomButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
