//
//  ResourceObjectProtocol.swift
//  AmaApp
//
//  Created by Li Lin2 on 12/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import Foundation

protocol ResourceObjectProtocol {
    
    var resourceObjectID : String { get }
    var resourceObjectType : String { get }
    
}
