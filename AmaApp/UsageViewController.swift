//
//  UsageViewController.swift
//  AmaApp
//
//  Created by Li Lin2 on 12/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import UIKit

class UsageViewController: UIViewController {
    
    @IBOutlet weak var dataBalanceLabel: UILabel!
    
    deinit {
        // NotificationCenter.default.removeObserver(self)
        // we don't need to remove observer: http://stackoverflow.com/a/40339926/1640653
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        NotificationCenter.default.addObserver(self, selector: #selector(updateView), name: Notification.Name(rawValue:Constants.NotoficationKeys.sharedViewModelChanged), object: nil)
    }
    
    func updateView() {
        if let dataBalance = SharedViewModel.sharedInstance.subscription?.includedDataBalance {
            let dataBalanceInGB = dataBalance / 1000.0
            // not divided by 1024: https://www.quora.com/Is-1-GB-equal-to-1024-MB-or-1000-MB
            dataBalanceLabel.text = String(format: "%.1fGB left", dataBalanceInGB)
        }
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: Constants.UserDefaultsKeys.isLoggedIn)
        
        let mainTabBarViewController = self.tabBarController as! MainTabBarViewController
        mainTabBarViewController.presentLoginViewController()
    }
    @IBAction func testCleanWelcomePageValue(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: Constants.UserDefaultsKeys.hasShownWelcomePage)
    }
}

