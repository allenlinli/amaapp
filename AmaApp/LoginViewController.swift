//
//  LoginViewController.swift
//  AmaApp
//
//  Created by Li Lin2 on 12/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBOutlet weak var mobileNumberTextField: UITextField!
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        if let mobileNumber = mobileNumberTextField.text {
            SharedViewModel.sharedInstance.getValues({ [unowned self] (tuple, error) in
                guard let msn = tuple?.1.msn else {
                    assert(false)
                }
                
                if mobileNumber != msn {
                    print("mobileNumber : \(mobileNumber), msn: \(msn)")
                    return 
                }
                
                // Success
                
                UserDefaults.standard.set(true, forKey: Constants.UserDefaultsKeys.isLoggedIn)
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
}
