//
//  APIManager.swift
//  AmaApp
//
//  Created by Li Lin2 on 12/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import Foundation

enum APIManagerError: Error {
    case parseJsonError
    case parseDataError
}

class APIManager : NSObject {
    
    public static let sharedInstance = APIManager()
    
    private (set) var config : URLSessionConfiguration
    private (set) var session : URLSession
    
    private override init() {
        config = URLSessionConfiguration.default
        session = URLSession(configuration: config)
    }
    
    public func getJsonValues(with completion: @escaping ((Account, Service, Subscription, [Product])?, _ error : Error?) -> Void) {
        getJsonDictionary { (jsonDictionary, error) in
            do {
                let (account, service, subscription, products) = try APIManager.parse(json: jsonDictionary)
                
                completion((account, service, subscription, products), nil)
            }
            catch let error {
                completion(nil,error)
            }
        }
    }
    
    func getJsonDictionary(with completion: @escaping (_  jsonDictionary: Dictionary<String, Any>, _ error : Error?) -> Void) {
        
        DispatchQueue.global().async { [unowned self] in
            // API call
            let testFakeURLString = "https://testFakeURLString.com"
            
            guard let url = URL(string: testFakeURLString) else {
                assert(false)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let task = self.session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                //mocking by using JSON file here
                
                guard let path = Bundle.main.path(forResource: "collection", ofType: "json") else {
                    assert(false)
                    return
                }
                
                // MOCK START
                let url = URL(fileURLWithPath: path)
                
                var jsonData : Data!
                do {
                    jsonData = try Data(contentsOf: url, options: .alwaysMapped)
                }
                catch let error {
                    assert(false, error.localizedDescription)
                    return
                }
                
                do {
                    let jsonDictionary =  try JSONSerialization.jsonObject(with: jsonData, options: []) as! Dictionary<String, Any>

                    DispatchQueue.main.async {
                        completion(jsonDictionary,nil)
                        return
                    }
                }
                catch let error {
                    assert(false, error.localizedDescription)
                    return
                }
                
                // MOCK END
                
                //            if let error = error {
                //                return
                //            }
                //            
                //            guard let data = data else {
                //                assert(false)
                //                return
                //            }
                
                //completion()
            })
            task.resume()
        }
    }
    
    static func parse(json dictionary: Dictionary<String, Any>) throws -> (Account, Service, Subscription, [Product]) {
        
        var account : Account!
        var service : Service!
        var subscription : Subscription!
        var products = [Product]()
        
        // MARK: parse "data"
//        guard let data = dictionary["data"] as? Dictionary<String, Any> else {
//            throw APIManagerError.parseJsonError
//        }
//        
//        guard let type = data["type"] as? String else {
//            throw APIManagerError.parseJsonError
//        }
//        assert(type == "accounts")
        account = Account()
        
        // MARK: parse "included"
        guard let included = dictionary["included"] as? Array<Dictionary<String, Any>> else {
            throw APIManagerError.parseJsonError
        }
        
        for includedObj in included {
            guard let type = includedObj["type"] as? String else {
                throw APIManagerError.parseJsonError
            }
            
            if type == "services" {
                guard let attributes = includedObj["attributes"] as? Dictionary<String, Any> else {
                    throw APIManagerError.parseJsonError
                }
                guard let msn = attributes["msn"] as? String, let credit = attributes["credit"] as? Int else {
                    throw APIManagerError.parseJsonError
                }
                
                service = Service(msn: msn, credit: credit)
            }
                
            else if type == "subscriptions" {
                guard let attributes = includedObj["attributes"] as? Dictionary<String, Any> else {
                    throw APIManagerError.parseJsonError
                }
                guard let includedDataBalance = attributes["included-data-balance"] as? Float else {
                    throw APIManagerError.parseJsonError
                }
                subscription = Subscription(includedDataBalance: includedDataBalance)
            }
                
            else if type == "products" {
                guard let attributes = includedObj["attributes"] as? Dictionary<String, Any> else {
                    throw APIManagerError.parseJsonError
                }
                
                guard let name = attributes["name"] as? String, let price = attributes["price"] as? Int else {
                    throw APIManagerError.parseJsonError
                }
                
                let product = Product(name: name, price: price)
                products.append(product)
            }
        }
        
        return (account, service, subscription, products)
    }
}
