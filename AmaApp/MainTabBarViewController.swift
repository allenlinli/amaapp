//
//  MainTabBarViewController.swift
//  AmaApp
//
//  Created by Li Lin2 on 13/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let isLoggedIn = UserDefaults.standard.value(forKey: Constants.UserDefaultsKeys.isLoggedIn) as? Bool
        if isLoggedIn != nil && isLoggedIn! == false  {
            presentLoginViewController()
            return
        }
        
        let hasShownWelcomePage = UserDefaults.standard.value(forKey: Constants.UserDefaultsKeys.hasShownWelcomePage) as? Bool
        if hasShownWelcomePage == nil || hasShownWelcomePage == false {
            guard let welcomeViewController = storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") else {
                assert(false)
            }
            UserDefaults.standard.set(true, forKey: Constants.UserDefaultsKeys.hasShownWelcomePage)
            present(welcomeViewController, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func presentLoginViewController() {
        guard let loginViewController = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") else {
            assert(false)
        }
        
        present(loginViewController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
