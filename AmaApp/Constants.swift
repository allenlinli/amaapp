//
//  Constants.swift
//  AmaApp
//
//  Created by Li Lin2 on 13/2/17.
//  Copyright © 2017 Raccoonism. All rights reserved.
//

import Foundation

struct Constants {
    struct UserDefaultsKeys {
        static let isLoggedIn = "isLoggedIn"
        static let hasShownWelcomePage = "hasShownWelcomePage"
    }
    
//    struct DataType {
//        static let account = "account"
//        static let service = "service"
//        static let subscription = "subscription"
//        static let product = "product"
//    }
    
    struct NotoficationKeys {
        static let sharedViewModelChanged = "sharedViewModelChanged"
    }
}
